# vendor_espressif

## 介绍

该仓库托管乐鑫科技公司开发的轻量物联网开发板样例代码。

## 软件架构

支持基于乐鑫科技ESP32_DEVKITC_V4开发板进行开发的样例。

代码路径：

```
vendor/espressif/                   --- vendor_espressif 仓库路径
└── esp32_wrover_ie_demo            --- esp32_wrover_ie样例
```

## 安装教程

参考 [安装调试教程](https://gitee.com/openharmony-sig/device_soc_espressif/blob/master/README.md)

## 使用说明

参考 [安装调试教程](https://gitee.com/openharmony-sig/device_soc_espressif/blob/master/README.md)

## 贡献

[如何参与](https://gitee.com/openharmony/docs/blob/HEAD/zh-cn/contribute/%E5%8F%82%E4%B8%8E%E8%B4%A1%E7%8C%AE.md)

[Commit message规范](https://gitee.com/openharmony/device_qemu/wikis/Commit%20message%E8%A7%84%E8%8C%83?sort_id=4042860)

## 相关仓

(https://gitee.com/openharmony-sig/vendor_espressif)
